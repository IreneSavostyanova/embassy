INSERT INTO public.citizen (id, first_name, last_name, passport_code, city_id, dob) VALUES (1, 'Viktor', 'Riazhen', 'EE545454', 6, '1990-10-13');
INSERT INTO public.citizen (id, first_name, last_name, passport_code, city_id, dob) VALUES (2, 'Vitalii', 'Push', 'EE123456', 4, '1980-09-24');
INSERT INTO public.citizen (id, first_name, last_name, passport_code, city_id, dob) VALUES (3, 'Mykola', 'Boiko', 'EE763297', 3, '2000-12-10');
INSERT INTO public.citizen (id, first_name, last_name, passport_code, city_id, dob) VALUES (5, 'Mariia', 'Tarasenko', 'EE215673', 16, '1982-03-12');
INSERT INTO public.citizen (id, first_name, last_name, passport_code, city_id, dob) VALUES (6, 'Ivan', 'Kulyk', 'EE904381', 13, '1990-01-12');
INSERT INTO public.citizen (id, first_name, last_name, passport_code, city_id, dob) VALUES (4, 'Larysa', 'Panasenko', 'EE909066', 5, '1999-05-14');