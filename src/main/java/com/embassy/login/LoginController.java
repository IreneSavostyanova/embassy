package com.embassy.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/embassy")
public class LoginController {

    @GetMapping("/login")
    public String getLoginPage() {
        return "login/login.html";
    }

    @PostMapping("/logout")
    public String logout() {
        return "redirect:/embassy/login";
    }
}
