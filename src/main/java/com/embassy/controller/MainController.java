package com.embassy.controller;

import com.embassy.object.Document;
import com.embassy.object.KindOfPermittingDocument;
import com.embassy.service.PermittingDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/embassy")
public class MainController {

    private final PermittingDocumentService permittingDocumentService;

    @Autowired
    public MainController(PermittingDocumentService permittingDocumentService) {
        this.permittingDocumentService = permittingDocumentService;
    }

    @GetMapping
    public String showMainPage(Model model) {
        model.addAttribute("permittingDocuments", permittingDocumentService.getPermittingDocuments());

        return "main";
    }

    @ResponseBody
    @GetMapping("/get_kind_of_permitting_document_by")
    public List<KindOfPermittingDocument> getKindsOfPermittingDocument(@RequestParam int id) {
        return permittingDocumentService.getKindsByDocumentId(id);
    }

    @ResponseBody
    @GetMapping("/view_documents")
    public List<Document> viewDocuments(@RequestParam int id) {
        return permittingDocumentService.viewDocuments(id);
    }
}
