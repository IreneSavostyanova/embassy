package com.embassy.controller;

import com.embassy.object.Citizen;
import com.embassy.object.City;
import com.embassy.object.IssuedDocument;
import com.embassy.object.dto.CitizenDto;
import com.embassy.service.CitizenService;
import com.embassy.service.CountryService;
import com.embassy.service.PermittingDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/embassy")
public class CitizenController {

    private final CitizenService citizenService;
    private final PermittingDocumentService permittingDocumentService;
    private final CountryService countryService;

    @Autowired
    public CitizenController(CitizenService citizenService,
                             CountryService countryService,
                             PermittingDocumentService permittingDocumentService) {

        this.citizenService = citizenService;
        this.countryService = countryService;
        this.permittingDocumentService = permittingDocumentService;
    }

    @GetMapping("/change_citizenship")
    public String showUpdatePage(Model model) {
        model.addAttribute("citizen", new CitizenDto());
        model.addAttribute("citizens", citizenService.getCitizens());
        model.addAttribute("documents", permittingDocumentService.getKindsForChangeCitizenship());
        model.addAttribute("countries", countryService.getCountries());

        return "changeCitizen";
    }

    @PostMapping("/change_citizenship")
    public String changeCitizenship(@ModelAttribute("citizen") CitizenDto citizen,
                                    @AuthenticationPrincipal UserDetails userDetails) {

        citizenService.updateCitizenship(citizen, userDetails.getUsername());

        return "redirect:/embassy";
    }

    @GetMapping("/issue_visa")
    public String showIssueVisaPage(Model model) {
        model.addAttribute("citizen", new CitizenDto());
        model.addAttribute("citizens", citizenService.getCitizens());
        model.addAttribute("documents",
                permittingDocumentService.getKindsForVisa());

        return "issueVisa";
    }

    @GetMapping("/info")
    public String showInfo(Model model) {
        model.addAttribute("citizens", citizenService.getCitizens());

        return "info";
    }

    @GetMapping("/get_citizen_info")
    @ResponseBody
    public Citizen getCitizen(@RequestParam int id) {
        return citizenService.getCitizenById(id);
    }

    @PostMapping("/issue_visa")
    public String issueVisa(@ModelAttribute("citizen") CitizenDto citizen,
                            @AuthenticationPrincipal UserDetails userDetails) {

        citizenService.issueVisa(citizen, userDetails.getUsername());

        return "redirect:/embassy";
    }

    @GetMapping("/get_cities_by_country")
    @ResponseBody
    public List<City> getCities(@RequestParam int id) {
        return countryService.getCitiesByCountryId(id);
    }

    @GetMapping("/get_citizen_visa")
    @ResponseBody
    public List<IssuedDocument> getVisas(@RequestParam int id) {
        return citizenService.getCitizenVisas(id);
    }
}
