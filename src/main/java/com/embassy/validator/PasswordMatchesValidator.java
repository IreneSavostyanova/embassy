package com.embassy.validator;


import com.embassy.registration.UserRegistrationForm;
import com.embassy.validator.annotation.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        UserRegistrationForm userRegistrationForm = (UserRegistrationForm) value;

        return userRegistrationForm.getPassword().equals(userRegistrationForm.getPasswordConfirmation());
    }
}
