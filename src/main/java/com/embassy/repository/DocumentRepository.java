package com.embassy.repository;

import com.embassy.object.Document;
import com.embassy.rowmapper.DocumentRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DocumentRepository {

    private static final String SQL_GET_BY_KIND = "SELECT d.* FROM document as d\n" +
            "LEFT JOIN document_in_set dis on d.id = dis.document_id\n" +
            "LEFT JOIN document_set ds on dis.document_set_id = ds.id\n" +
            "WHERE ds.kind_of_permitting_doc_id = :kind_id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DocumentRowMapper documentRowMapper;

    @Autowired
    public DocumentRepository(NamedParameterJdbcTemplate jdbcTemplate,
                              DocumentRowMapper documentRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.documentRowMapper = documentRowMapper;
    }

    public List<Document> getDocumentsByKindOfPermittingDocument(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("kind_id", id);

        return jdbcTemplate.query(SQL_GET_BY_KIND, map, documentRowMapper);
    }
}
