package com.embassy.repository;

import com.embassy.object.PermittingDocument;
import com.embassy.rowmapper.PermittingDocumentRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PermittingDocumentRepository {

    private static final String SQL_GET_BY_ID = "SELECT * FROM permitting_document where id = :id";
    private static final String SQL_GET_ALL = "SELECT * FROM permitting_document";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final PermittingDocumentRowMapper permittingDocumentRowMapper;

    public PermittingDocumentRepository(NamedParameterJdbcTemplate jdbcTemplate,
                                        PermittingDocumentRowMapper permittingDocumentRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.permittingDocumentRowMapper = permittingDocumentRowMapper;
    }

    public PermittingDocument getPermittingDocumentById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_BY_ID, map, permittingDocumentRowMapper);
    }

    public List<PermittingDocument> getPermittingDocuments() {
        return jdbcTemplate.query(SQL_GET_ALL, permittingDocumentRowMapper);
    }
}
