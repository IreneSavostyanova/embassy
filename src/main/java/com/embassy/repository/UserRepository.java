package com.embassy.repository;

import com.embassy.object.Role;
import com.embassy.object.User;
import com.embassy.rowmapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class UserRepository {

    private static final String SQL_SAVE_USER = "INSERT INTO users (first_name, last_name, password_hash, username) " +
            "VALUES (:first_name, :last_name, :password_hash, :username)";
    private static final String SQL_GET_USER = "SELECT * FROM users WHERE username = ?";
    private static final String SQL_GET_BY_ID = "SELECT * FROM users WHERE id = :id";
    private static final String SQL_ASSIGN_ADMIN = "INSERT INTO user_role (user_id, role_id) VALUES (:user_id, :role_id)";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserRowMapper userRowMapper;

    @Autowired
    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate,
                          UserRowMapper userRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.userRowMapper = userRowMapper;
    }

    public User getUserById(int user_id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", user_id);

        return jdbcTemplate.queryForObject(SQL_GET_BY_ID, map, userRowMapper);
    }

    public int saveUser(User user) {
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("username", user.getUsername())
                .addValue("password_hash", user.getPassword())
                .addValue("first_name", user.getFirstName())
                .addValue("last_name", user.getLastName());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(SQL_SAVE_USER,
                sqlParameterSource,
                keyHolder, new String[]{"id"});
        return Objects.requireNonNull(keyHolder.getKey()).intValue();
    }

    public User getUserByUsername(String username) {
        try {
            return jdbcTemplate.getJdbcTemplate()
                    .queryForObject(SQL_GET_USER, userRowMapper, username);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public void assignUserRole(User user, Role role) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("user_id", user.getId())
                .addValue("role_id", role.getId());

        jdbcTemplate.update(SQL_ASSIGN_ADMIN, map);
    }
}
