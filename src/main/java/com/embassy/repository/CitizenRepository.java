package com.embassy.repository;

import com.embassy.object.Citizen;
import com.embassy.rowmapper.CitizenRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CitizenRepository {

    private static final String SQL_GET_CITIZEN_BY_ID = "SELECT * FROM citizen WHERE id = :id";
    private static final String SQL_GET_ALL = "SELECT * FROM citizen";
    private static final String SQL_UPDATE_CITIZENSHIP = "UPDATE citizen SET city_id = :city_id WHERE id = :id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CitizenRowMapper citizenRowMapper;

    @Autowired
    public CitizenRepository(NamedParameterJdbcTemplate jdbcTemplate,
                             CitizenRowMapper citizenRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.citizenRowMapper = citizenRowMapper;
    }

    public Citizen getCitizenById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_CITIZEN_BY_ID, map, citizenRowMapper);
    }

    public List<Citizen> getCitizens() {
        return jdbcTemplate.query(SQL_GET_ALL, citizenRowMapper);
    }

    public void updateCitizenship(Citizen citizen) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("city_id", citizen.getCity().getId())
                .addValue("id", citizen.getId());
        jdbcTemplate.update(SQL_UPDATE_CITIZENSHIP, map);
    }
}
