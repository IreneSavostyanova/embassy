package com.embassy.repository;

import com.embassy.object.City;
import com.embassy.rowmapper.CityRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityRepository {

    private static final String SQL_GET_CITY_BY_ID = "SELECT * FROM city WHERE id = :id";
    private static final String SQL_GET_BY_COUNTRY_ID = "SELECT * FROM city where country_id = :country_id";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CityRowMapper cityRowMapper;

    @Autowired
    public CityRepository(NamedParameterJdbcTemplate jdbcTemplate,
                          CityRowMapper cityRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.cityRowMapper = cityRowMapper;
    }

    public City getCityById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_CITY_BY_ID, map, cityRowMapper);
    }

    public List<City> getCitiesByCountryId(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("country_id", id);

        return jdbcTemplate.query(SQL_GET_BY_COUNTRY_ID, map, cityRowMapper);
    }
}
