package com.embassy.repository;

import com.embassy.object.KindOfPermittingDocument;
import com.embassy.rowmapper.KindOfPermittingDocumentRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KindOfPermittingDocumentRepository {

    private static final String SQL_GET_BY_ID = "SELECT * FROM kind_of_permitting_document WHERE id = :id";
    private static final String SQL_GET_KINDS_BY_DOCUMENT_ID = "SELECT * FROM kind_of_permitting_document WHERE permitting_document_id = :permitting_document_id";
    private static final String SQL_GET_FOR_CITIZENSHIP = "SELECT * FROM kind_of_permitting_document " +
            "WHERE permitting_document_id = (SELECT id FROM permitting_document WHERE permitting_document.name = 'Citizenship')";
    private static final String SQL_GET_FOR_VISA = "SELECT * FROM kind_of_permitting_document " +
            "WHERE permitting_document_id = (SELECT id FROM permitting_document WHERE permitting_document.name = 'Visa')";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final KindOfPermittingDocumentRowMapper rowMapper;

    public KindOfPermittingDocumentRepository(NamedParameterJdbcTemplate jdbcTemplate,
                                              KindOfPermittingDocumentRowMapper rowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    public KindOfPermittingDocument getKindById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_BY_ID, map, rowMapper);
    }

    public List<KindOfPermittingDocument> getKindsByPermittingDocument(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("permitting_document_id", id);

        return jdbcTemplate.query(SQL_GET_KINDS_BY_DOCUMENT_ID, map, rowMapper);
    }

    public List<KindOfPermittingDocument> getKindsForChangeCitizenship() {
        return jdbcTemplate.query(SQL_GET_FOR_CITIZENSHIP, rowMapper);
    }

    public List<KindOfPermittingDocument> gteKindsForVisa() {
        return jdbcTemplate.query(SQL_GET_FOR_VISA, rowMapper);
    }
}
