package com.embassy.repository;

import com.embassy.object.IssuedDocument;
import com.embassy.rowmapper.IssuedDocumentRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IssuedDocumentRepository {

    private static final String SQL_MARK_ISSUED_DOC = "INSERT INTO issued_documents " +
            "(citizen_id, kind_of_permitting_doc_id, date_of_issue, officer_id) " +
            "VALUES (:citizen_id, :kind_id, :date_of_issue, :officer_id)";
    private static final String SQL_GET_CITIZEN_VISAS = "SELECT * FROM issued_documents iss\n" +
            "LEFT JOIN kind_of_permitting_document kopd on iss.kind_of_permitting_doc_id = kopd.id\n" +
            "WHERE iss.citizen_id = :citizen_id\n" +
            "  AND  kopd.permitting_document_id = (SELECT id FROM permitting_document p where p.name = 'Visa');";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final IssuedDocumentRowMapper issuedDocumentRowMapper;

    @Autowired
    public IssuedDocumentRepository(NamedParameterJdbcTemplate jdbcTemplate,
                                    IssuedDocumentRowMapper issuedDocumentRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.issuedDocumentRowMapper = issuedDocumentRowMapper;
    }

    public void markIssuedDocument(IssuedDocument issuedDocument) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("citizen_id", issuedDocument.getCitizen().getId())
                .addValue("kind_id", issuedDocument.getKindOfPermittingDocument().getId())
                .addValue("date_of_issue", issuedDocument.getDateOfIssue())
                .addValue("officer_id", issuedDocument.getUser().getId());

        jdbcTemplate.update(SQL_MARK_ISSUED_DOC, map);
    }

    public List<IssuedDocument> getCitizenVisas(int citizenId) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("citizen_id", citizenId);

        return jdbcTemplate.query(SQL_GET_CITIZEN_VISAS, map, issuedDocumentRowMapper);
    }
}
