package com.embassy.repository;

import com.embassy.object.Country;
import com.embassy.rowmapper.CountryRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepository {

    private static final String SQL_GET_COUNTRY_BY_ID = "SELECT * FROM country WHERE id = :id";
    private static final String SQL_GET_ALL = "SELECT * FROM country";
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CountryRowMapper countryRowMapper;

    @Autowired
    public CountryRepository(NamedParameterJdbcTemplate jdbcTemplate,
                             CountryRowMapper countryRowMapper) {

        this.jdbcTemplate = jdbcTemplate;
        this.countryRowMapper = countryRowMapper;
    }

    public Country getCountryById(int id) {
        SqlParameterSource map = new MapSqlParameterSource()
                .addValue("id", id);

        return jdbcTemplate.queryForObject(SQL_GET_COUNTRY_BY_ID, map, countryRowMapper);
    }

    public List<Country> getCountries() {
        return jdbcTemplate.query(SQL_GET_ALL, countryRowMapper);
    }
}
