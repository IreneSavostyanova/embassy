package com.embassy.service;

import com.embassy.object.City;
import com.embassy.object.Country;
import com.embassy.repository.CityRepository;
import com.embassy.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {

    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository,
                          CityRepository cityRepository) {

        this.countryRepository = countryRepository;
        this.cityRepository = cityRepository;
    }

    public List<Country> getCountries() {
        return countryRepository.getCountries();
    }

    public List<City> getCitiesByCountryId(int id) {
        return cityRepository.getCitiesByCountryId(id);
    }
}
