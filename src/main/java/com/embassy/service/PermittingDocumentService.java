package com.embassy.service;

import com.embassy.object.Document;
import com.embassy.object.KindOfPermittingDocument;
import com.embassy.object.PermittingDocument;
import com.embassy.repository.DocumentRepository;
import com.embassy.repository.KindOfPermittingDocumentRepository;
import com.embassy.repository.PermittingDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermittingDocumentService {

    private final PermittingDocumentRepository permittingDocumentRepository;
    private final KindOfPermittingDocumentRepository kindOfPermittingDocumentRepository;
    private final DocumentRepository documentRepository;

    @Autowired
    public PermittingDocumentService(PermittingDocumentRepository permittingDocumentRepository,
                                     KindOfPermittingDocumentRepository kindOfPermittingDocumentRepository,
                                     DocumentRepository documentRepository) {

        this.permittingDocumentRepository = permittingDocumentRepository;
        this.kindOfPermittingDocumentRepository = kindOfPermittingDocumentRepository;
        this.documentRepository = documentRepository;
    }

    public List<PermittingDocument> getPermittingDocuments() {
        return permittingDocumentRepository.getPermittingDocuments();
    }

    public List<KindOfPermittingDocument> getKindsByDocumentId(int id) {
        return kindOfPermittingDocumentRepository.getKindsByPermittingDocument(id);
    }

    public List<Document> viewDocuments(int kindId) {
        return documentRepository.getDocumentsByKindOfPermittingDocument(kindId);
    }

    public List<KindOfPermittingDocument> getKindsForChangeCitizenship() {
        return kindOfPermittingDocumentRepository.getKindsForChangeCitizenship();
    }

    public List<KindOfPermittingDocument> getKindsForVisa() {
        return kindOfPermittingDocumentRepository.gteKindsForVisa();
    }
}
