package com.embassy.service;

import com.embassy.object.Role;
import com.embassy.object.User;
import com.embassy.registration.UserRegistrationForm;
import com.embassy.repository.RoleRepository;
import com.embassy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       PasswordEncoder passwordEncoder) {

        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Transactional
    public void register(UserRegistrationForm userRegistrationForm) {
        User user = createNewUser(userRegistrationForm);
        int id = userRepository.saveUser(user);
        Role role = roleRepository.getRoleByName("ADMIN");
        user.setId(id);
        roleRepository.addRoleToUser(user, role);
    }

    private User createNewUser(UserRegistrationForm userRegistrationForm) {
        User user = new User();
        user.setUsername(userRegistrationForm.getUsername());
        user.setFirstName(userRegistrationForm.getFirstName());
        user.setLastName(userRegistrationForm.getLastName());
        user.setPassword(passwordEncoder.encode(userRegistrationForm.getPassword()));

        return user;
    }
}
