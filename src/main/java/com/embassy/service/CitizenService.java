package com.embassy.service;

import com.embassy.object.Citizen;
import com.embassy.object.IssuedDocument;
import com.embassy.object.dto.CitizenDto;
import com.embassy.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class CitizenService {

    private final CitizenRepository citizenRepository;
    private final CityRepository cityRepository;
    private final IssuedDocumentRepository issuedDocumentRepository;
    private final UserRepository userRepository;
    private final KindOfPermittingDocumentRepository kindOfPermittingDocumentRepository;

    @Autowired
    public CitizenService(CitizenRepository citizenRepository,
                          CityRepository cityRepository,
                          IssuedDocumentRepository issuedDocumentRepository,
                          UserRepository userRepository,
                          KindOfPermittingDocumentRepository kindOfPermittingDocumentRepository) {

        this.citizenRepository = citizenRepository;
        this.cityRepository = cityRepository;
        this.issuedDocumentRepository = issuedDocumentRepository;
        this.userRepository = userRepository;
        this.kindOfPermittingDocumentRepository = kindOfPermittingDocumentRepository;
    }

    public List<Citizen> getCitizens() {
        return citizenRepository.getCitizens();
    }

    @Transactional
    public void updateCitizenship(CitizenDto citizenDto,
                                  String officerUsername) {

        Citizen citizen = new Citizen();
        citizen.setId(citizenDto.getId());
        citizen.setCity(cityRepository.getCityById(citizenDto.getCityId()));
        citizenRepository.updateCitizenship(citizen);
        IssuedDocument issuedDocument = new IssuedDocument();
        issuedDocument.setCitizen(citizen);
        issuedDocument.setDateOfIssue(LocalDate.now().toString());
        issuedDocument.setUser(userRepository.getUserByUsername(officerUsername));
        issuedDocument.setKindOfPermittingDocument(
                kindOfPermittingDocumentRepository.getKindById(
                        citizenDto.getKindOfPermittingDocumentId()
                )
        );
        issuedDocumentRepository.markIssuedDocument(issuedDocument);
    }

    @Transactional
    public void issueVisa(CitizenDto citizenDto, String officerUsername) {
        Citizen citizen = new Citizen();
        citizen.setId(citizenDto.getId());
        IssuedDocument issuedDocument = new IssuedDocument();
        issuedDocument.setCitizen(citizen);
        issuedDocument.setDateOfIssue(LocalDate.now().toString());
        issuedDocument.setUser(userRepository.getUserByUsername(officerUsername));
        issuedDocument.setKindOfPermittingDocument(
                kindOfPermittingDocumentRepository.getKindById(
                        citizenDto.getKindOfPermittingDocumentId()
                )
        );
        issuedDocumentRepository.markIssuedDocument(issuedDocument);
    }

    public Citizen getCitizenById(int id) {
        return citizenRepository.getCitizenById(id);
    }

    public List<IssuedDocument> getCitizenVisas(int citizenId) {
        return issuedDocumentRepository.getCitizenVisas(citizenId);
    }
}
