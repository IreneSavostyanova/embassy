package com.embassy.registration;

import com.embassy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/embassy")
public class UserRegistrationController {

    private final UserService userService;

    @Autowired
    public UserRegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String getRegistrationForm(Model model) {
        model.addAttribute("userRegistrationForm", new UserRegistrationForm());

        return "/registration/registration.html";
    }

    @PostMapping("/registration")
    public String register(@ModelAttribute("userRegistrationForm") @Valid UserRegistrationForm userRegistrationForm,
                           BindingResult bindingResult,
                           Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("userRegistrationForm", userRegistrationForm);

            return "/registration/registration.html";
        }
        userService.register(userRegistrationForm);

        return "redirect:/embassy";
    }
}

