package com.embassy.rowmapper;

import com.embassy.object.IssuedDocument;
import com.embassy.repository.CitizenRepository;
import com.embassy.repository.KindOfPermittingDocumentRepository;
import com.embassy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class IssuedDocumentRowMapper implements RowMapper<IssuedDocument> {

    private final KindOfPermittingDocumentRepository kindOfPermittingDocumentRepository;
    private final UserRepository userRepository;
    private final CitizenRepository citizenRepository;

    @Autowired
    public IssuedDocumentRowMapper(KindOfPermittingDocumentRepository kindOfPermittingDocumentRepository,
                                   UserRepository userRepository,
                                   CitizenRepository citizenRepository) {

        this.kindOfPermittingDocumentRepository = kindOfPermittingDocumentRepository;
        this.userRepository = userRepository;
        this.citizenRepository = citizenRepository;
    }

    @Override
    public IssuedDocument mapRow(ResultSet rs, int rowNum) throws SQLException {
        IssuedDocument issuedDocument = new IssuedDocument();
        issuedDocument.setId(rs.getInt("id"));
        issuedDocument.setDateOfIssue(rs.getString("date_of_issue"));
        issuedDocument.setKindOfPermittingDocument(
                kindOfPermittingDocumentRepository.getKindById(
                        rs.getInt("kind_of_permitting_doc_id"))
        );
        issuedDocument.setUser(userRepository.getUserById(rs.getInt("officer_id")));
        issuedDocument.setCitizen(citizenRepository.getCitizenById(rs.getInt("citizen_id")));

        return issuedDocument;
    }
}
