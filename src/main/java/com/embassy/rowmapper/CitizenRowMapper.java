package com.embassy.rowmapper;

import com.embassy.object.Citizen;
import com.embassy.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CitizenRowMapper implements RowMapper<Citizen> {

    private final CityRepository cityRepository;

    @Autowired
    public CitizenRowMapper(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public Citizen mapRow(ResultSet rs, int rowNum) throws SQLException {
        Citizen citizen = new Citizen();
        citizen.setId(rs.getInt("id"));
        citizen.setFirstName(rs.getString("first_name"));
        citizen.setLastName(rs.getString("last_name"));
        citizen.setPassportCode(rs.getString("passport_code"));
        citizen.setDob(rs.getString("dob"));
        citizen.setCity(cityRepository.getCityById(rs.getInt("city_id")));

        return citizen;
    }
}
