package com.embassy.rowmapper;

import com.embassy.object.PermittingDocument;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PermittingDocumentRowMapper implements RowMapper<PermittingDocument> {

    @Override
    public PermittingDocument mapRow(ResultSet rs, int rowNum) throws SQLException {
        PermittingDocument permittingDocument = new PermittingDocument();
        permittingDocument.setId(rs.getInt("id"));
        permittingDocument.setName(rs.getString("name"));

        return permittingDocument;
    }
}
