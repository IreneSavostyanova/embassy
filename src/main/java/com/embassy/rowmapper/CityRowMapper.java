package com.embassy.rowmapper;

import com.embassy.object.City;
import com.embassy.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CityRowMapper implements RowMapper<City> {

    private final CountryRepository countryRepository;

    @Autowired
    public CityRowMapper(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public City mapRow(ResultSet rs, int rowNum) throws SQLException {
        City city = new City();
        city.setId(rs.getInt("id"));
        city.setName(rs.getString("name"));
        city.setCountry(countryRepository.getCountryById(rs.getInt("country_id")));

        return city;
    }
}
