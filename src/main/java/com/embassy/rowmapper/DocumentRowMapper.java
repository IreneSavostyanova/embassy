package com.embassy.rowmapper;

import com.embassy.object.Document;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class DocumentRowMapper implements RowMapper<Document> {

    @Override
    public Document mapRow(ResultSet rs, int rowNum) throws SQLException {
        Document document = new Document();
        document.setId(rs.getInt("id"));
        document.setName(rs.getString("name"));
        document.setDescription(rs.getString("description"));

        return document;
    }
}
