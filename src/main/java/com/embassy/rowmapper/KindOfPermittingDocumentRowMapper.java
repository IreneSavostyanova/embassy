package com.embassy.rowmapper;

import com.embassy.object.KindOfPermittingDocument;
import com.embassy.repository.PermittingDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class KindOfPermittingDocumentRowMapper implements RowMapper<KindOfPermittingDocument> {

    private final PermittingDocumentRepository permittingDocumentRepository;

    @Autowired
    public KindOfPermittingDocumentRowMapper(PermittingDocumentRepository permittingDocumentRepository) {
        this.permittingDocumentRepository = permittingDocumentRepository;
    }

    @Override
    public KindOfPermittingDocument mapRow(ResultSet rs, int rowNum) throws SQLException {
        KindOfPermittingDocument kindOfPermittingDocument = new KindOfPermittingDocument();
        kindOfPermittingDocument.setId(rs.getInt("id"));
        kindOfPermittingDocument.setName(rs.getString("name"));
        kindOfPermittingDocument.setDescription(rs.getString("description"));
        kindOfPermittingDocument.setPermittingDocument(
                permittingDocumentRepository.getPermittingDocumentById(
                        rs.getInt("permitting_document_id"))
        );

        return kindOfPermittingDocument;
    }
}
