package com.embassy.object;

public class IssuedDocument {

    private int id;
    private Citizen citizen;
    private KindOfPermittingDocument kindOfPermittingDocument;
    private User user;
    private String dateOfIssue;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Citizen getCitizen() {
        return citizen;
    }

    public void setCitizen(Citizen citizen) {
        this.citizen = citizen;
    }

    public KindOfPermittingDocument getKindOfPermittingDocument() {
        return kindOfPermittingDocument;
    }

    public void setKindOfPermittingDocument(KindOfPermittingDocument kindOfPermittingDocument) {
        this.kindOfPermittingDocument = kindOfPermittingDocument;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(String dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }
}
