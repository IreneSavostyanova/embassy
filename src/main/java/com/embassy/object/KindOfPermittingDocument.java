package com.embassy.object;

public class KindOfPermittingDocument {

    private int id;
    private String name;
    private String description;
    private PermittingDocument permittingDocument;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PermittingDocument getPermittingDocument() {
        return permittingDocument;
    }

    public void setPermittingDocument(PermittingDocument permittingDocument) {
        this.permittingDocument = permittingDocument;
    }
}
