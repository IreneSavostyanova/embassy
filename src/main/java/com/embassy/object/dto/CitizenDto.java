package com.embassy.object.dto;

public class CitizenDto {

    private int id;
    private String firstName;
    private String lastName;
    private String passportCode;
    private String dob;
    private int cityId;
    private int kindOfPermittingDocumentId;


    public int getKindOfPermittingDocumentId() {
        return kindOfPermittingDocumentId;
    }

    public void setKindOfPermittingDocumentId(int kindOfPermittingDocumentId) {
        this.kindOfPermittingDocumentId = kindOfPermittingDocumentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassportCode() {
        return passportCode;
    }

    public void setPassportCode(String passportCode) {
        this.passportCode = passportCode;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
