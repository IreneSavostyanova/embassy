-- auto-generated definition
create table user_role
(
  id      serial not null
    constraint user_role_pk
      primary key,
  role_id integer
    constraint user_role_role_id_fk
      references role
      on update cascade on delete cascade,
  user_id integer
    constraint user_role_users_id_fk
      references users
      on update cascade on delete cascade
);

alter table user_role
  owner to postgres;

create unique index user_role_id_uindex
  on user_role (id);

