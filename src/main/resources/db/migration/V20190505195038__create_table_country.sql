-- auto-generated definition
create table country
(
  id   serial not null
    constraint country_pk
      primary key,
  name varchar
);

alter table country
  owner to postgres;

create unique index country_id_uindex
  on country (id);