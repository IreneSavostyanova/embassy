-- auto-generated definition
create table document
(
  id          serial not null
    constraint document_pk
      primary key,
  name        varchar,
  description varchar
);

alter table document
  owner to postgres;

create unique index document_id_uindex
  on document (id);

