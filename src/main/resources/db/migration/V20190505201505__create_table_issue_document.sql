-- auto-generated definition
create table issued_documents
(
  id                        serial not null
    constraint citizen_visas_pk
      primary key,
  citizen_id                integer
    constraint citizen_visas_citizen_id_fk
      references citizen
      on update cascade on delete cascade,
  kind_of_permitting_doc_id integer
    constraint citizen_visas_kind_of_permitting_document_id_fk
      references kind_of_permitting_document
      on update cascade on delete cascade,
  date_of_issue             varchar,
  officer_id                integer
    constraint issued_documents_users_id_fk
      references users
);

alter table issued_documents
  owner to postgres;

create unique index citizen_visas_id_uindex
  on issued_documents (id);

