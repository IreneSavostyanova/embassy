-- auto-generated definition
create table permitting_document
(
  id   serial not null
    constraint permitting_document_pk
      primary key,
  name varchar
);

alter table permitting_document
  owner to postgres;

create unique index permitting_document_id_uindex
  on permitting_document (id);

