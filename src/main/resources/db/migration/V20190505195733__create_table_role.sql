-- auto-generated definition
create table role
(
  id   serial not null
    constraint role_pk
      primary key,
  name varchar
);

alter table role
  owner to postgres;

create unique index role_id_uindex
  on role (id);

