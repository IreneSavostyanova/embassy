-- auto-generated definition
create table users
(
  id            serial not null
    constraint users_pk
      primary key,
  username      varchar,
  password_hash varchar,
  first_name    varchar,
  last_name     varchar
);

alter table users
  owner to postgres;

create unique index users_id_uindex
  on users (id);

