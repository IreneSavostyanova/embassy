-- auto-generated definition
create table citizen
(
  id            serial not null
    constraint citizen_pk
      primary key,
  first_name    varchar,
  last_name     varchar,
  passport_code varchar,
  city_id       integer
    constraint citizen_city_id_fk
      references city
      on update cascade on delete cascade,
  dob           varchar
);

alter table citizen
  owner to postgres;

create unique index citizen_id_uindex
  on citizen (id);

