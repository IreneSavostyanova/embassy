-- auto-generated definition
create table document_set
(
  id                        serial not null
    constraint document_set_pk
      primary key,
  kind_of_permitting_doc_id integer
    constraint document_set_kind_of_permitting_document_id_fk
      references kind_of_permitting_document
);

alter table document_set
  owner to postgres;

create unique index document_set_id_uindex
  on document_set (id);

