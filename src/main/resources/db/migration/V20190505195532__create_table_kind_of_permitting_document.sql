-- auto-generated definition
create table kind_of_permitting_document
(
  id                     serial not null
    constraint kind_of_permitting_document_pk
      primary key,
  name                   varchar,
  description            varchar,
  permitting_document_id integer
    constraint kind_of_permitting_document_permitting_document_id_fk
      references permitting_document
      on update cascade on delete cascade
);

alter table kind_of_permitting_document
  owner to postgres;

create unique index kind_of_permitting_document_id_uindex
  on kind_of_permitting_document (id);

