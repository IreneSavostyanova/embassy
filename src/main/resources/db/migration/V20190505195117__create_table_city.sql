-- auto-generated definition
create table city
(
  id         serial not null
    constraint city_pk
      primary key,
  name       varchar,
  country_id integer
    constraint city_country_id_fk
      references country
      on update cascade on delete cascade
);

alter table city
  owner to postgres;

create unique index city_id_uindex
  on city (id);

