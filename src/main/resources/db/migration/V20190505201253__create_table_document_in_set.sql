-- auto-generated definition
create table document_in_set
(
  id              serial not null
    constraint document_in_set_pk
      primary key,
  document_set_id integer
    constraint document_in_set_document_set_id_fk
      references document_set
      on update cascade on delete cascade,
  document_id     integer
    constraint document_in_set_document_id_fk
      references document
      on update cascade on delete cascade
);

alter table document_in_set
  owner to postgres;

create unique index document_in_set_id_uindex
  on document_in_set (id);

